import axios from "axios-observable";
import { API_URL } from "@/common/config";

const ApiService = {
  init() {
    // axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded';
    axios.defaults.headers.post['Accept'] = 'application/json';
  },

  setHeader() {
    // axios.defaults.headers.common['Authorization'] = `Bearer ${TokenService.getToken()}`
    axios.defaults.headers.post['Accept'] = 'application/json';
    // axios.defaults.headers.post['Content-Type'] = 'application/json';
  },

  query(resource, params) {
    return axios.get(resource, params);
  },

  get(resource, slug = "", params) {
    return axios.get(`${API_URL}${resource}${slug}`, {params: params});
  },

  overrideGet(resource, slug = "", params) {
    return axios.get(`${resource}${slug}`, {params: params});
  },

  post(resource, params, header) {
    return axios.post(`${API_URL}${resource}`, params, header);
  },

  update(resource, slug, params) {
    return axios.put(`${API_URL}${resource}/${slug}`, params);
  },

  put(resource, params, headers) {
    return axios.put(`${API_URL}${resource}`, params, headers);
  },

  delete(resource) {
    return axios.delete(`${API_URL}${resource}`);
  },

  overridePut(resource, params) {
    let headers = {
      'Content-Type': 'text/xml'
    };
    return axios.put(`${resource}`, params, headers);
  },

  overridePost(resource, params) {
    let headers = {
      'Content-Type': 'text/xml'
    };
    return axios.post(`${resource}`, params, headers);
  },
};

export default ApiService;

// export const ProblemsetsService = {
//   get (slug) {
//     return ApiService.get("problemsets", slug)
//   },
//   create (params) {
//     return ApiService.post('articles', {article: params})
//   },
//   update (slug, params) {
//     return ApiService.update('articles', slug, {article: params})
//   },
//   destroy (slug) {
//     return ApiService.delete(`articles/${slug}`)
//   }
// }