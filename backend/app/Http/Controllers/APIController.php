<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class APIController extends Controller
{
    private $urlBase = "http://localhost:8080/";

    public function createScanner(Request $request, $table) {
        // $ch = curl_init();

        // curl_setopt($ch, CURLOPT_URL, 'http://localhost:8080/version/cluster');
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        
        
        // $headers = array();
        // $headers[] = 'Accept: text/xml';
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        // $result = curl_exec($ch);
        // if (curl_errno($ch)) {
        //     echo 'Error:' . curl_error($ch);
        // }
        // curl_close ($ch);
        // return $result;


        //===========
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'http://localhost:8080/data/scanner');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "<Scanner batch=\"1\"/>");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');


        $headers = array();
        $headers[] = 'Accept: text/xml';
        $headers[] = 'Content-Type: text/xml';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);

        $headerResult = [];
        $header = rtrim($result);
        $data = explode("\n",$header);
        $headerResult['status'] = $data[0];
        array_shift($data);

        foreach($data as $part){

            //some headerResult will contain ":" character (Location for example), and the part after ":" will be lost, Thanks to @Emanuele
            $middle = explode(":",$part,2);

            //Supress warning message if $middle[1] does not exist, Thanks to @crayons
            if ( !isset($middle[1]) ) { $middle[1] = null; }

            $headerResult[trim($middle[0])] = trim($middle[1]);
        }

        // $response = curl_exec($curl);
        // $err = curl_error($curl);
        // curl_close($curl);
        // echo $result;
        // var_dump($result);
        // var_dump(curl_exec($ch));
        return $headerResult;
    }

    public function test() {
        return 'asdfasdfasdf';
    }

    public function createTable(Request $request, $table) {
        $xml = $request->input('xmlRequest');
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'http://localhost:8080/' . $table . '/schema');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = 'Accept: text/xml';
        $headers[] = 'Content-Type: text/xml';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        var_dump("asdfasfadsf");
        return $result;
    }
}
